package impl

import (
	"context"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/ds_2/go-ytgaming/api"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// CreateLoginUrl creates a login URL for the YouTube Gaming API using the provided configuration.
//
// ytgConfig is the configuration for the YouTube Gaming API client.
// redirectUrl is the URL to redirect the user to after authentication.
// state is a unique identifier for the authentication request.
// scopes are the OAuth scopes to request access for.
//
// The function returns the login URL that the user should be redirected to for authentication.
func CreateLoginUrl(ytgConfig *api.YoutubeGamingClientData, redirectUrl string, state string, scopes []string) string {
	//ctx := context.Background()
	conf := &oauth2.Config{
		ClientID:     ytgConfig.ClientId,
		ClientSecret: ytgConfig.ClientSecret,
		RedirectURL:  redirectUrl,
		Scopes:       scopes,
		Endpoint:     google.Endpoint,
	}
	loginWebUrl := conf.AuthCodeURL(state, oauth2.AccessTypeOffline)
	logrus.Debugln("Url to authenticate is", loginWebUrl)
	return loginWebUrl
}

// ParseAuthCode exchanges an authorization code for an OAuth2 token and returns an HTTP client
// configured with the obtained tokens.
//
// ytgConfig is the configuration for the YouTube Gaming API client.
// redirectUrl is the URL to redirect the user to after authentication.
// authCode is the authorization code obtained from the OAuth2 flow.
// scopes are the OAuth scopes to request access for.
//
// The function returns an HTTP client that can be used to make authenticated requests to the
// YouTube Gaming API.
func ParseAuthCode(ytgConfig *api.YoutubeGamingClientData, redirectUrl string, authCode string, scopes []string) *http.Client {
	conf := &oauth2.Config{
		ClientID:     ytgConfig.ClientId,
		ClientSecret: ytgConfig.ClientSecret,
		RedirectURL:  redirectUrl,
		Scopes:       scopes,
		Endpoint:     google.Endpoint,
	}
	tok, err := conf.Exchange(context.Background(), authCode)
	if err != nil {
		logrus.Fatalln(err)
	}
	logrus.Debugln("Token value:", &tok)
	ytgConfig.AccessToken = tok.AccessToken
	ytgConfig.RefreshToken = tok.RefreshToken
	logrus.Infoln("Tokens: a=", tok.AccessToken, ", r=", tok.RefreshToken, ", exp=", tok.Expiry)
	client := conf.Client(context.Background(), tok)
	return client
}

// CreateClientFromTokens creates an HTTP client configured with the provided access and refresh tokens.
//
// ytgConfig is the configuration for the YouTube Gaming API client, which contains the access and refresh tokens.
// scopes are the OAuth scopes to request access for.
//
// The function returns an HTTP client that can be used to make authenticated requests to the YouTube Gaming API.
func CreateClientFromTokens(ytgConfig *api.YoutubeGamingClientData, scopes []string) *http.Client {
	tokenSource := oauth2.Token{
		AccessToken:  ytgConfig.AccessToken,
		RefreshToken: ytgConfig.RefreshToken,
		TokenType:    "Bearer",
	}
	conf := &oauth2.Config{
		ClientID:     ytgConfig.ClientId,
		ClientSecret: ytgConfig.ClientSecret,
		Scopes:       scopes,
		Endpoint:     google.Endpoint,
	}
	client := conf.Client(context.Background(), &tokenSource)
	return client
}
