package impl

// MY_SCOPES is a slice of strings representing the OAuth2 scopes required for the YouTube API.
// These scopes grant read-only access to the user's YouTube account, as well as the ability
// to upload videos on their behalf.
var MY_SCOPES = []string{"https://www.googleapis.com/auth/youtube.readonly", "https://www.googleapis.com/auth/youtube.upload", "https://www.googleapis.com/auth/youtube"}

// V3_LIVEBROADCASTS is the URL for the YouTube Data API v3 liveBroadcasts endpoint, which provides access to live broadcast resources.
var V3_LIVEBROADCASTS = "https://www.googleapis.com/youtube/v3/liveBroadcasts"
