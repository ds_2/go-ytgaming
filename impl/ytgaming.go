package impl

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/ds_2/go-ytgaming/api"
)

// GetLiveBroadcasts retrieves a list of live YouTube broadcasts based on the provided parameters.
// The function takes an HTTP client and a GetBroadcastRequest object as input, and returns a GetBroadcastResponse object and an error.
// The GetBroadcastRequest object can be used to filter the results by various criteria, such as maximum results, broadcast status, and broadcast IDs.
// The function sends a GET request to the YouTube LiveBroadcasts API endpoint and unmarshals the response into the GetBroadcastResponse object.
func GetLiveBroadcasts(client *http.Client, params *api.GetBroadcastRequest) (api.GetBroadcastResponse, error) {
	var urlParamsMap = make(url.Values, 10)
	urlParamsMap.Set("part", "id,status,contentDetails,snippet")
	if params.MaxResults > 0 {
		urlParamsMap.Set("maxResults", strconv.Itoa(int(params.MaxResults)))
	}
	if params.Mine {
		urlParamsMap.Set("mine", "true")
	}
	if params.GetStatus() != api.BroadcastStatus_not_set {
		urlParamsMap.Del("mine")
		urlParamsMap.Set("broadcastStatus", params.Status.String())
	}
	if len(params.Ids) > 0 {
		urlParamsMap.Del("mine")
		urlParamsMap.Del("broadcastStatus")
		urlParamsMap.Set("id", strings.Join(params.Ids, ","))
	}
	//part=id&part=status&broadcastStatus=all&broadcastType=all&maxResults=20&mine=true
	broadcastApiUrl := fmt.Sprintf("https://www.googleapis.com/youtube/v3/liveBroadcasts?%s",
		urlParamsMap.Encode())
	logrus.Debugln("Using url:", broadcastApiUrl)
	response, err := client.Get(broadcastApiUrl)
	if err != nil {
		return api.GetBroadcastResponse{}, err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			//ignore
		}
	}(response.Body)
	bodyStr, err := ioutil.ReadAll(response.Body)
	logrus.Debugln("Response body as string: ", string(bodyStr))
	var j api.GetBroadcastResponse
	err = json.Unmarshal(bodyStr, &j)
	if err != nil {
		panic(err)
	}
	logrus.Debugln("As Object: ", j)
	return j, nil
}

func DeleteBroadcast(client *http.Client, id string) {
	broadcastApiUrl := V3_LIVEBROADCASTS
	logrus.Debugln("Using url:", broadcastApiUrl)
	deleteRequest, _ := http.NewRequest("DELETE", broadcastApiUrl, nil)
	response, err := client.Do(deleteRequest)
	if err != nil {
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			//ignore
		}
	}(response.Body)
}
