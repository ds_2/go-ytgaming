package tests

import (
	"os"
	"testing"

	"gitlab.com/ds_2/go-ytgaming/api"
	"gitlab.com/ds_2/go-ytgaming/impl"
)

func TestCreateLoginUrl(t *testing.T) {
	var clientData = api.YoutubeGamingClientData{
		ClientId:     os.Getenv("ONAIR_CLIENT_ID"),
		ClientSecret: os.Getenv("ONAIR_CLIENT_SECRET"),
	}
	url := impl.CreateLoginUrl(&clientData, "http://localhost:9000", "rState2", impl.MY_SCOPES)
	if len(url) <= 0 {
		t.Fail()
	}
	t.Log("Url is ", url)
}
