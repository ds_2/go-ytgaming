# Golang Youtube Gaming Client

API from

* <https://developers.google.com/youtube/v3/live/docs/>
* <https://developers.google.com/youtube/?hl=de>

## Build it

    protoc --go_out=api api/*.proto
    go build --race ./...

## Wording

A broadcast is a scheduled event. You can create more than one.

A stream is a data stream of binary audio and video content. A stream is associated to a broadcast.

A cuepoint is a time mark where ads can be injected by YT into the broadcast.
