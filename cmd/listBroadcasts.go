/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/ds_2/go-ytgaming/api"
	"gitlab.com/ds_2/go-ytgaming/impl"
)

// listBroadcastsCmd represents the listBroadcasts command
var listBroadcastsCmd = &cobra.Command{
	Use:   "listBroadcasts",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		var clientConfig = &api.YoutubeGamingClientData{
			ClientId:     os.Getenv("ONAIR_CLIENT_ID"),
			ClientSecret: os.Getenv("ONAIR_CLIENT_SECRET"),
			AccessToken:  os.Getenv("ONAIR_YT_ACCESSTOKEN"),
			RefreshToken: os.Getenv("ONAIR_YT_REFRESHTOKEN"),
		}
		var httpClient *http.Client
		if len(clientConfig.AccessToken) > 0 {
			logrus.Infoln("Accesstoken given, will use this one!")
			//access token
			httpClient = impl.CreateClientFromTokens(clientConfig, impl.MY_SCOPES)
		} else {
			logrus.Infoln("No access token found, will use web login flow..")
			var rdUrl = "http://localhost:9000"
			loginUrl := impl.CreateLoginUrl(clientConfig, rdUrl, "rState1", impl.MY_SCOPES)
			logrus.Infoln("Login url is ", loginUrl)
			var code string
			if _, err := fmt.Scan(&code); err != nil {
				logrus.Fatal(err)
			}
			httpClient = impl.ParseAuthCode(clientConfig, rdUrl, code, impl.MY_SCOPES)
		}

		if httpClient == nil {
			logrus.Fatal("No httpClient!")
		}
		var params = api.GetBroadcastRequest{
			//Status:     api.BroadcastStatus_all,
			MaxResults: 20,
			Mine:       true,
		}
		myLiveBroadcasts, _ := impl.GetLiveBroadcasts(httpClient, &params)
		logrus.Infoln("My live broadcasts: ", &myLiveBroadcasts)
	},
}

func init() {
	rootCmd.AddCommand(listBroadcastsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// listBroadcastsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// listBroadcastsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
